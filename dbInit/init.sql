-- phpMyAdmin SQL
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Oct 15, 2020 at 11:15 AM
-- Server version: 10.5.6-MariaDB-1:10.5.6+maria~focal
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `prog2053-proj`
--
CREATE DATABASE IF NOT EXISTS `prog2053-proj` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `prog2053-proj`;

-- --------------------------------------------------------

--
-- Table structure for table `blockedcomments`
--

CREATE TABLE `blockedcomments` (
  `bid` bigint(8) NOT NULL,
  `user` bigint(8) NOT NULL,
  `reason` varchar(20000) COLLATE utf8_bin DEFAULT NULL,
  `date` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `blockedcomments`
--

INSERT INTO `blockedcomments` (`bid`, `user`, `reason`, `date`) VALUES
(2, 1, 'This comment was stupid, I should fire this moderator', '2020-10-15'),
(3, 1, 'This post was stupid :)', '2020-10-15');

-- --------------------------------------------------------

--
-- Table structure for table `blockedposts`
--

CREATE TABLE `blockedposts` (
  `bid` bigint(8) NOT NULL,
  `user` bigint(8) NOT NULL,
  `reason` varchar(20000) COLLATE utf8_bin DEFAULT NULL,
  `date` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `blockedposts`
--

INSERT INTO `blockedposts` (`bid`, `user`, `reason`, `date`) VALUES
(3, 1, 'This post was stupid :)', '2020-10-15');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `cid` bigint(8) NOT NULL,
  `post` bigint(8) NOT NULL,
  `user` bigint(8) NOT NULL,
  `comment` varchar(20000) COLLATE utf8_bin DEFAULT NULL,
  `date` date NOT NULL DEFAULT current_timestamp(),
  `likes` int(11) DEFAULT 0,
  `adminException` tinyint(1) NOT NULL DEFAULT 0,
  `blockedID` bigint(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`cid`, `post`, `user`, `comment`, `date`, `likes`, `adminException`, `blockedID`) VALUES
(1, 1, 2, 'Possimus nesciunt dicta neque. Sint voluptatem sequi aliquid voluptas beatae. Sed incidunt ad voluptas ut facere. Molestiae id qui commodi molestiae mollitia dolorum voluptatem eos.', '2020-10-15', 0, 0, NULL),
(2, 2, 1, 'Modi ut et soluta deserunt. Saepe qui nesciunt illum quis in est. Quia dignissimos tenetur nam accusantium accusantium vitae libero.', '2020-10-15', 0, 1, 2),
(3, 3, 3, 'Repudiandae officia tempora dolore illum. Quis perspiciatis fugiat maiores inventore. Ut sit et velit debitis doloribus error. Culpa reiciendis soluta dolores libero fugit explicabo.', '2020-10-15', 0, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `pid` bigint(8) NOT NULL,
  `user` bigint(8) NOT NULL,
  `title` varchar(200) COLLATE utf8_bin NOT NULL,
  `content` varchar(20000) COLLATE utf8_bin NOT NULL,
  `date` date NOT NULL DEFAULT current_timestamp(),
  `likes` int(11) DEFAULT 0,
  `adminException` tinyint(1) NOT NULL DEFAULT 0,
  `blockedID` bigint(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`pid`, `user`, `title`, `content`, `date`, `likes`, `adminException`, `blockedID`) VALUES
(1, 3, 'Animi ut occaecati omnis iure magnam aliquam quam.', 'Dolorum beatae porro autem et possimus qui eum. Et facilis soluta quo distinctio. Voluptatem quam quia fugiat quaerat dolore aut. Autem autem aut minus quia optio.', '2020-10-15', 0, 0, NULL),
(2, 2, 'Nobis laboriosam totam labore aut possimus pariatur recusandae.', 'Accusantium unde dignissimos quia ab quas corporis. Impedit possimus rerum ut ratione qui et a. Illum cum fugit atque.', '2020-10-15', 0, 0, NULL),
(3, 1, 'Optio consequuntur sint tempore molestiae aut esse.', 'Modi quia eius natus consectetur ab ut. Animi facilis quam placeat. Illo nulla autem ut qui voluptate aut.', '2020-10-15', 0, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `uid` bigint(8) NOT NULL,
  `email` varchar(128) COLLATE utf8_bin NOT NULL,
  `password` varchar(128) COLLATE utf8_bin NOT NULL,
  `userType` enum('admin','moderator','user') COLLATE utf8_bin NOT NULL DEFAULT 'user',
  `picture` longblob DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `email`, `password`, `userType`, `picture`) VALUES
(1, 'bartell.martine@example.com', '40bcc6f6193986153cae1bb1c36668650a3d5f97', 'admin', NULL),
(2, 'zcrona@example.net', '1f66d81577cd95514cedc8504d65ec8eff9c336a', 'moderator', NULL),
(3, 'wgaylord@example.com', '3fcba21eebd2d09681515b4849d2bbeae566451e', 'user', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blockedcomments`
--
ALTER TABLE `blockedcomments`
  ADD PRIMARY KEY (`bid`),
  ADD KEY `user` (`user`) USING BTREE;

--
-- Indexes for table `blockedposts`
--
ALTER TABLE `blockedposts`
  ADD PRIMARY KEY (`bid`),
  ADD KEY `user` (`user`) USING BTREE;

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`cid`),
  ADD KEY `user` (`user`),
  ADD KEY `post` (`post`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`pid`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blockedcomments`
--
ALTER TABLE `blockedcomments`
  MODIFY `bid` bigint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `blockedposts`
--
ALTER TABLE `blockedposts`
  MODIFY `bid` bigint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `cid` bigint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `pid` bigint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `uid` bigint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`uid`),
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`post`) REFERENCES `posts` (`pid`);

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`uid`);
COMMIT;
