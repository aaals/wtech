
import { css, html, LitElement } from "lit-element";


export class HomeView extends LitElement {
	
	static styles = css`
	body {
		background-color: #E8ECEF;
	  }
	  
	  .centered {
		  padding-top: 200px;
		  admin text-align: center;
	  }
	  
	  .secret-text {
		text-align: center;
		font-size: 2rem;
		color: #fff;
		background-color: #000;
	  }
	`;

	/**
	 * Rendering of the the home page, in html-code/html-format.
	 */
	render() {
		return html`
		<div class="jumbotron centered">
		<div class="container">
		  <i class="fas fa-key fa-6x"></i>
		  <h1 class="display-3">XYZ Forum</h1>
		  <p class="lead">The back alley of the internet</p>
		  <hr>
		  <a class="btn btn-light btn-lg" href="/register" role="button">Register</a>
		  <a class="btn btn-dark btn-lg" href="/login" role="button">Login</a>
		  <hr>
		  <a class="btn btn-dark btn-lg" href="/forum" role="button">Access forum</a>
		  <p class="lead">Must be logged in first!</p>
		</div>
		`;
	}

}

customElements.define("view-home", HomeView);