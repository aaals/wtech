import { css, html, LitElement } from "lit-element";

export class RegisterView extends LitElement {
	
	static styles = css`
	body {
		background-color: #E8ECEF;
	  }
	  
	  .centered {
		  padding-top: 200px;
		  text-align: center;
	  }
	`;

	/**
	 * Getting properties used in the registration process as strings.
	 */
	static get properties() {
		return {
			path: {type: String},
			successPath: {type: String},
			email: {type: String},
			password: {type: String},
			confirmPassword: {type: String}
		};
	}

	/**
	 * Constructor for applying the properties to the relevant session.
	 */
	constructor() {
		super();
		this.path = `${window.MyAppGlobals.serverURL}auth/register`
		this.successPath = "http://localhost:8080/login";
		this.email = "";
		this.password = "";
		this.confirmPassword = "";
	}

	/**
	 * Rendering of the the registration page, in html-code/html-format.
	 */
	render() {
		return html`
		<div class="container mt-5">
		<h1>Login</h1>
	  
		<div class="row">
		  <div class="col-sm-8">
			<div class="card">
			  <div class="card-body">
	  
				<!-- Makes POST request to /login route -->
				<form @submit="${(event) => this.submit(event)}">

					<label for="email">Email</label>
					<input class="form-control" 
						type="email"
						name="Email"
						placeholder="example@website.com"
						required
						@change="${(event) => this.updateEmail(event)}"
					/>

					<label for="password">Password</label>
					<input class="form-control"
						type="password" 
						name="Password"
						required
						@change="${(event) => this.updatePassword(event)}"
					/>

					<label for="password">Confirm Password</label>
					<input class="form-control"
						type="password" 
						name="Password"
						required
						@change="${(event) => this.updateConfirmPassword(event)}"
					/>
					<br></br>
					<button class="btn btn-dark" type="submit">
						Register
					</button>

				</form>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
		`;
	}

	/**
	 * Asyncronous function for submitting the registration information, and waiting for the inputs to be made and checked.
	 * 
	 * @param {*} event	- Refers to changes made during the registration process, such as input of email and password.
	 */
	async submit(event) {
		event.preventDefault();

		if (this.password !== this.confirmPassword) {
			window.alert("Passwords do not match!");
		} else {
			const form =  {
				email: this.email,
				password: this.password
			};
	
			const parcel = {
				method: "POST",
				credentials: "include",
				headers: { "Content-Type": "application/json" },
				body: JSON.stringify(form)
			};
	
			var status = await fetch(this.path, parcel); 
			console.log(status);
			if (status.ok) {
				window.location.href = this.successPath;
			} else {
				window.alert("This email is already in use!");
			}
		}
	}

	/**
	 * Function for updating the email's string value.
	 * 
	 * @param {*} event Refers to changes made during the publishing process, in this case the email string's value.
	 */
	updateEmail(event) {
		event.preventDefault();
		this.email = event.target.value;
	}

	/**
	 * Function for updating the password's string value.
	 * 
	 * @param {*} event Refers to changes made during the publishing process, in this case the password string's value.
	 */
	updatePassword(event) {
		event.preventDefault();
		this.password = event.target.value;
	}

	/**
	 * Function for updating the password confirmation's (repeat password's) string value.
	 * 
	 * @param {*} event Refers to changes made during the publishing process, in this case the password confirmation string's value.
	 */
	updateConfirmPassword(event) {
		event.preventDefault();
		this.confirmPassword = event.target.value;
	}
}

customElements.define("view-register", RegisterView);