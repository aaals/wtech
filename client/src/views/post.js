
import { css, html, LitElement } from "lit-element";


export class PostView extends LitElement {
	
	static styles = css`
	body {
		background-color: #E8ECEF;
	  }
	  
	  .centered {
		  padding-top: 200px;
		  text-align: center;
	  }
	`;

	/**
	 * Getting properties used in the posting process, including "metadata", as strings.
	 */
	static get properties() {
		return {
			postNav: {type: String},
			ownerNav: {type: String},
			forumNav: {type: String},
			postOwner: {type: String},
			post: { 
				type: {
					user: String,
					title: String,
					content: String,
					date: String
				}
			}
		};
	}

	/**
	 * Constructor for applying the properties to the relevant session.
	 */
	constructor() {
		super();
		// Paths
		this.postNav = `http://localhost:8081/get/post/`;
		this.ownerNav = `http://localhost:8081/get/user/`;
		this.forumNav = "/forum";
		this.post = {};
		this.postOwner = {};
	}

	/**
	 * Rendering of the the post view page, in html-code/html-format.
	 */
	render() {
		return html`
			<div class="jumbotron centered">
				<div class="container">

				<a class="btn btn-dark btn-lg" href="/forum" role="button">Back to Forum</a>

				<!-- Post Content Column -->
				<div class="col-lg-8">
		  
				  <!-- Title -->
				  <h1 class="mt-4">${this.post.title}</h1>
		  
				  <!-- Author -->
				  <p class="lead">
					by
					<a href="#">${this.postOwner.email}</a>
				  </p>
		  
				  <hr>
		  
				  <!-- Date/Time -->
				  <p>Posted on ${this.post.date}</p>
		  
				  <hr>
		  
				  <!-- Post Content -->
				  <p>${this.post.content}</p>
		  
				  <hr>
		  
				  <!-- Comments Form -->
				  <div class="card my-4">
					<h5 class="card-header">Leave a Comment:</h5>
					<div class="card-body">
					  <form>
						<div class="form-group">
						  <textarea class="form-control" rows="3"></textarea>
						</div>
						<button type="submit" class="btn btn-primary">Submit</button>
					  </form>
					</div>
				  </div>
		  
				  <!-- Single Comment -->
				  <div class="media mb-4">
					<img class="d-flex mr-3 rounded-circle">
					<div class="media-body">
					  <h5 class="mt-0">Commenter Name</h5>
					  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
					</div>
				  </div>
		  
				</div>
				</div>
	    	</div>
		`;
	}

	/**
	 * Asyncronous function checking for updates to the post.
	 * 
	 * @param {*} changedProperties - Eventual changed properties that are parsed into the function.
	 */
	async update(changedProperties) {
		
		const pid = document.location.href.slice(27);
		
		const postParcel = {
			method: "POST",
			credentials: "include",
			headers: {Accept: "application/json"},
			pid: pid
		};
		
		// Scuffed way of getting the post id hahah
		// Can't get that value from location, seems like it needs to be passed
		// from the router

		var post = await fetch(`http://localhost:8081/get/post/${pid}`, postParcel);
		this.post = await post.json();
		console.log(this.post);
		
		var ownerParcel = {
			method: "POST",
			credentials: "include",
			headers: {Accept: "application/json"},
			uid: this.post.user
		};


		var owner = await fetch(`http://localhost:8081/get/user/${this.post.user}`, ownerParcel);
		this.postOwner = await owner.json();
		console.log(this.postOwner);

		super.update(changedProperties);
	}

}

customElements.define("view-post", PostView);