
import { css, html, LitElement } from "lit-element";


export class PublishView extends LitElement {
	
	static styles = css`
	body {
		background-color: #E8ECEF;
	  }
	  
	  .centered {
		  padding-top: 200px;
		  text-align: center;
	  }
	`;

	/**
	 * Getting properties used in the publishing process (posting/commenting) as strings.
	 */
	static get properties() {
		return {
			path: {type: String},	// 
			forumNav: {type: String}, // 
			title: {type: String}, //
			content: {type: String}
		};
	}

	/**
	 * Constructor for applying the properties to the relevant session.
	 */
	constructor() {
		super();
		// Paths
		this.path = "http://localhost:8081/post/newPost";
		this.successPath = "/forum";
		this.homeNav = "/";

		this.title = "";
		this.content = "";
	}

	/**
	 * Rendering of the the login page, in html-code/html-format.
	 */
	render() {
		return html`
		<div class="container mt-5">
		<h1>Create New Post</h1>
	  
		<div class="row">
		  <div class="col-sm-8">
			<div class="card">
			  <div class="card-body">
	  
				<!-- Makes POST request to /post route -->
				<form @submit="${(event) => this.submit(event)}">

				<label for="post">Post Title</label>
					<input class="form-control" 
						type="post"
						name="post"
						placeholder="Title"
						minlength="1" 
						maxlength="100"
						required
						@change="${(event) => this.updateTitle(event)}"
					/>

					<label for="postBody">Post</label>
					<textarea class="form-control"
						type="post" 
						name="postBody"
						placeholder="Share your secrets..."
						maxlength="2000"
						required
						@change="${(event) => this.updatePassword(event)}">
					</textarea>
					<br></br>
					<button class="btn btn-dark" type="submit">
						Submit
					</button>
					<br></br>
					<a class="btn btn-dark btn-lg" href="/forum" role="button">Back to Forum</a>
				</form>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
		`;
	}

	/**
	 * Asyncronous function for submitting the publishing information, and waiting for the inputs to be made and checked.
	 * 
	 * @param {*} event	- Refers to changes made during the posting/commenting process, such as input of title and post contents.
	 */
	async submit(event) {
		event.preventDefault();

		const form =  {
			title: this.title,
			content: this.conent
		};

		const parcel = {
			method: "POST",
			credentials: "include",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify(form)
		};

		var status = await fetch(this.path, parcel); 
		console.log(status);
		if (status.ok) {
			window.location.href = this.successPath;
		} else {
			window.alert("Could not publish your post")
		}
	}

	/**
	 * Function for updating the title's string value.
	 * 
	 * @param {*} event - Refers to changes made during the publishing process, in this case the title string's value.
	 */
	updateTitle(event) {
		event.preventDefault();
		this.title = event.target.value;
	}

	/**
	 * Function for updating the post content's string value.
	 * 
	 * @param {*} event - Refers to changes made during the publishing process, in this case the content string's value.
	 */
	updateContent(event) {
		event.preventDefault();
		this.content = event.target.value;
	}
}

customElements.define("view-publish", PublishView);