import { css, html, LitElement } from "lit-element";

export class LoginView extends LitElement {
	
	static styles = css`
	body {
		background-color: #E8ECEF;
	  }
	  
	  .centered {
		  padding-top: 200px;
		  text-align: center;
	  }
	`;

	/**
	 * Getting properties used in the login process as strings.
	 */
	static get properties() {
		return {
			path: {type: String},
			successPath: {type: String},
			email: {type: String},
			password: {type: String}
		};
	}

	/**
	 * Constructor for applying the properties to the relevant session.
	 */
	constructor() {
		super();
		this.path = `${window.MyAppGlobals.serverURL}auth/login`
		this.successPath = "http://localhost:8080/forum";
		this.email = "";
		this.password = "";
	}

	/**
	 * Rendering of the the login page, in html-code/html-format.
	 */
	render() {
		return html`
		<div class="container mt-5">
		<h1>Login</h1>
	  
		<div class="row">
		  <div class="col-sm-8">
			<div class="card">
			  <div class="card-body">
	  
				<!-- Makes POST request to /login route -->
				<form @submit="${(event) => this.submit(event)}">

					<label for="email">Email</label>
					<input class="form-control" 
						type="email"
						name="Email"
						placeholder="example@website.com"
						required
						@change="${(event) => this.updateEmail(event)}"
					/>

					<label for="password">Password</label>
					<input class="form-control"
						type="password" 
						name="Password"
						required
						@change="${(event) => this.updatePassword(event)}"
					/>
					<br></br>
					<button class="btn btn-dark" type="submit">
						Login
					</button>
					
				</form>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
		`;
	}

	/**
	 * Asyncronous function for submitting the information, and waiting for the inputs to be made and checked.
	 * 
	 * @param {*} event	- Refers to changes made during the login process, such as input of email and password.
	 */
	async submit(event) {
		event.preventDefault();

		const form =  {
			email: this.email,
			password: this.password
		};

		const parcel = {
			method: "POST",
			credentials: "include",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify(form)
		};

		var status = await fetch(this.path, parcel); 
		console.log(status);
		if (status.ok) {
			window.location.href = this.successPath;
		} else {
			window.alert("Email or Password incorrect");
		}
	}

	/**
	 * Function for updating the email's string value.
	 * 
	 * @param {*} event Refers to changes made during the login process, in this case the email string's value.
	 */
	updateEmail(event) {
		event.preventDefault();
		this.email = event.target.value;
	}

	/**
	 * Function for updating the passwprd's string value.
	 * 
	 * @param {*} event Refers to changes made during the login process, in this case the password string's value.
	 */
	updatePassword(event) {
		event.preventDefault();
		this.password = event.target.value;
	}

}

customElements.define("view-login", LoginView);