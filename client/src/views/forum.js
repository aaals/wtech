
import { css, html, LitElement } from "lit-element";


export class ForumView extends LitElement {
	
	static styles = css`
	body {
		background-color: #E8ECEF;
	  }
	  
	  .centered {
		  padding-top: 200px;
		  text-align: center;
	  }
	`;

	/**
	 * Getting properties used in the login process as strings.
	 */
	static get properties() {
		return {
			path: {type: String},	// 
			loginPath: {type: String},
			logoutNav: {type: String}, // 
			newPostNav: {type: String}, //
			posts: {type: Array}
		};
	}

	/**
	 * Constructor for applying the properties to the relevant session.
	 */
	constructor() {
		super();
		// Paths
		this.findPostNav = "http://localhost:8081/get/allPosts";
		this.newPostNav = "/post";
		this.logoutNav = "/logout";
		this.loginPath = "/login";

		this.posts = [];
	}

	/**
	 * Rendering of the the forum page, in html-code/html-format.
	 */
	render() {
		return html`
			<div class="jumbotron centered">
				<div class="container">
		  			<i class="fas fa-key fa-6x"></i>
		  				<h1 class="display-3">XYZ Forum</h1>
		  				<p class="lead">The back alley of the internet</p>
					  <hr>
					  <a class="btn btn-dark btn-lg" href="/logout" role="button">Log out</a>
		  			<br><br>
		  				<a class="btn btn-dark btn-lg" href="/publish" role="button">Create New Post</a>
					  <br><br>

					  ${this.posts.map((post) => {
						return html`
							<div class="container d-inline">
								<div class="card text-center" style="width: 100%;">
									<div class="card-body">
										  <h5 class="card-title">${post.title}</h5>
										 <a href="/post/${post.pid}" class="btn btn-dark">View</a>
									</div>
								  </div>
							</div>
						`;
					})}

				</div>
	    	</div>
		`;
	}

	/**
	 * Asyncronous function for submitting the information, and waiting for the inputs to be made and checked.
	 * 
	 * @param {*} event Refers to changes made while on the forum page, such as the user deciding to view a post.
	 */
	async update(changedProperties) {
		const parcel = {
			method: "GET",
			credentials: "include",
			headers: {Accept: "application/json"}
		};

		var result = await fetch(this.findPostNav, parcel);
		
		if(result) {
			result = await result.json();
			this.posts = result;
			console.log(this.posts);

			if (this.posts == null) {
				this.posts = [];
			}		

			super.update(changedProperties);
		} else {
			window.location.href = this.loginPath;
		}
	}
}

customElements.define("view-forum", ForumView);