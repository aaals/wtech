
import { css, html, LitElement } from "lit-element";


export class LogoutView extends LitElement {
	
	static styles = css`
	body {
		background-color: #E8ECEF;
	  }
	  
	  .centered {
		  padding-top: 200px;
		  text-align: center;
	  }
	  
	  .secret-text {
		text-align: center;
		font-size: 2rem;
		color: #fff;
		background-color: #000;
	  }
	`;

	/**
	 * Constructor for applying the properties to the relevant session.
	 */
	constructor() {
		super();
	}

	/**
	 * Rendering of the the logout page, in html-code/html-format.
	 */
	render() {
		return html`
		<div class="jumbotron text-center">
		<div class="container">
		
		  <i class="fas fa-key fa-6x"></i>
		  <h1 class="display-3">You are logged out!</h1>

		  <a class="btn btn-dark btn-lg" href="/" role="button">Home</a>
		
		  </div>
	  	</div>
		`;
	}

	/**
	 * Asyncronous function waiting for the logout functionality before alerting the user of them logging out, and rerouting them.
	 */
	async update() {
		// Not finished
		const parcel = {
			method: "POST",
			credentials: "include"
		};
	
		await fetch("http://localhost:8081/auth/logout", parcel)
		console.log(status);
		
		window.alert("You are now logged out");

		super.update();
	}

}

customElements.define("view-logout", LogoutView);