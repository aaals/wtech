import { Router } from "@vaadin/router";

import './views/home.js';
import './views/register.js';
import './views/login.js';
import './views/logout.js';
import './views/forum.js';
import './views/post.js';
import './views/publish.js';

const outlet = document.getElementById("outlet");
const router = new Router(outlet);

/**
 * Setting/linking the routes to their relevant view-elements.
 */
router.setRoutes([
    { path: "/", component: "view-home" },
    { path: "/login", component: "view-login" },
    { path: "/forum", component: "view-forum" },
    { path: "/logout", component: "view-logout" },
    { path: "/post/:pid", component: "view-post" },
    { path: "/publish", component: "view-publish" },
    { path: "/register", component: "view-register" }
]);