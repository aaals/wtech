//"use strict";

// for using `require` in an ESM
import { createRequire } from 'module';
const require = createRequire(import.meta.url);

import express from 'express';        // Viewport
import bodyParser from 'body-parser'; // HTML-JSON handler
import cors from 'cors';

// Import routes
import auth from './src/routes/auth.js';
import get from './src/routes/get.js';
import post from './src/routes/post.js';
import put from './src/routes/put.js';

import cookieParser from 'cookie-parser';

const app = express();
const PORT = 8081;


app.listen(PORT, () => {
  console.log('Running...');
});

// Check cors on npm for more options
const corsOptions = {
  // I'm using a VM but opening the page from my host (origin can be different so I accept all)
  origin: 'http://localhost:8080',
  methods: 'GET,PUT,POST,DELETE',
  credentials: true,
	preflightContinue: false
};

app.use(cors(corsOptions)); // Cross origin resource middleware between server/client
app.use(express.json()); // Parse JSON bodies (as sent by API clients)
app.use(cookieParser()); // Parse cookies
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ // Parse URL-encoded bodies (as sent by HTML forms)
  extended: true
}));


// Route definition

// GET route
app.use('/get', get);

// POST route
app.use('/post', post);

// PUT route
app.use('/put', put);

// Authentication (Registration, login, logout)
app.use('/auth', auth);