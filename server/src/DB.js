// This file includes connection details to the DB
// Include in any js module that needs a DB connection
import mysql from 'promise-mysql';            // DB driver

/**
 * Creating a connection to the database.
 */
let db = mysql.createConnection({
    host: "db",
    user: "admin",
    password: "password",
    database: 'prog2053-proj'
});

/*
db.connect(function (err) {
    if (err) {
      throw err;
    }
    console.log("Connected!");
});
*/

export default db;