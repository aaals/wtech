import express from 'express';
import verify from '../verify-token.js';
import { createComment, createPost } from '../controllers/create.js';

var post = express.Router();


////////////////////////////////////////
//////////////// POSTS /////////////////
////////////////////////////////////////

/**
 * Insert a new post into the DB.
 */
post.all("/newPost", verify, async (req, res, err) => {
  try {
    await createPost(
      req.body.title,
      req.body.content,
      req.user.uid
    ).then;
    return res.status(200).send();

  } catch { err } {
    console.log(err);
    res.status(500).send(err);
  }
});

////////////////////////////////////////
/////////////// COMMENTS ///////////////
////////////////////////////////////////

/**
 * Insert a new comment into the DB.
 */
post.all("/newComment", verify, async (req, res, err) => {
  try {
    await createComment(
      req.body.post,
      req.user.uid,
      req.body.comment
    );
    return res.status(200).send();
  } catch { err } {
    console.log(err);
    res.status(500).send(err);
  }
});


////////////////////////////////////////
/////////////// BLOCKED ////////////////
////////////////////////////////////////


export default post;