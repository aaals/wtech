import express from 'express';
import verify from '../verify-token.js'; // Login verification
import { blockPost, unblockPost } from '../controllers/edit.js';

var put = express.Router();


////////////////////////////////////////
//////////////// BLOCKED ///////////////
////////////////////////////////////////

/**
 * Block a post in the DB.
 */
put.all("/blockPost", verify, async (req, res, err) => {
  try {
    await blockPost(req.package.pid);
    return res.status(200).send();
  } catch { err } {
    console.log(err);
    res.status(500).send(err);
  }
});

/**
 * Unblock a post in the DB.
 */
put.all("/unblockPost", verify, async (req, res, err) => {
  try {
    await unblockPost(req.package.pid);
    return res.status(200).send();
  } catch { err } {
    console.log(err);
    res.status(500).send(err);
  }
});


export default put;