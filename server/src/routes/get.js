import express from 'express';
import verify from '../verify-token.js';
import { findCommentsWithID, findPostWithID, findTableWithName, findUserWithEmail, findUserWithID } from '../controllers/find.js';

var get = express.Router();


////////////////////////////////////////
/////////////// USERS //////////////////
////////////////////////////////////////

/**
 *  Request all users profile details.
 */
get.all("/allUsers", async (res, err) => {
  try {
    const users = await findTableWithName("users");
    return res.status(200).append("Content-Type", "application/json").end(JSON.stringify(users));
  } catch { err } {
    console.log(err);
    res.status(500).send(err);
  }
});


/**
 * Request a user's profile details by providing an ID.
 */
get.all("/user/:uid", async (req, res, err) => {
  try {
    const profile = await findUserWithID(req.params.uid).then();
    if (profile) {
      // If found return the user
      return res.status(200).append("Content-Type", "application/json").end(JSON.stringify(profile));
    } else {
      // Returns a not found status
      return res.status(404).send();
    }
  } catch { err } {
    console.log(err);
    res.status(500).send(err);
  }
});

////////////////////////////////////////
/////////////// POSTS //////////////////
////////////////////////////////////////

/**
 * Request all posts from the DB.
 */
get.all("/allPosts", verify, async (req, res) => {
	try {
    const table = await findTableWithName("posts").then();
    console.log(table);
    if (!table) {
      res.status(404).append("Content-Type", "application/json").end(null);
    } else {
      res.status(200).append("Content-Type", "application/json").end(JSON.stringify(table));
    }
	} catch (err) {
		console.log(err);
		res.status(500).send(err);
	}
});

/**
 * Request a post's details by providing an ID.
 */
get.all("/post/:pid", async (req, res, err) => {
  try {
    const post = await findPostWithID(req.params.pid);
    console.log(post);
    if (post) {
      // If found return the post
      return res.status(200).append("Content-Type", "application/json").end(JSON.stringify(post));
    } else {
      // Returns a not found status
      return res.status(404).send();
    }
  } catch { err } {
    console.log(err);
    res.status(500).send(err);
  }
});

/**
 * Request the user table's details by providing the table's name.
 */
get.all("/allUsers", async (res, err) => {
  try {
    const users = await findTableWithName("users");
    return res.status(200).append("Content-Type", "application/json").end(JSON.stringify(users));
  } catch { err } {
    console.log(err);
    res.status(500).send(err);
  }
});

////////////////////////////////////////
/////////////// COMMENTS ///////////////
////////////////////////////////////////

/**
 * Request all comments from the DB.
 */
get.all("/allComments", async (res, err) => {
  try {
    const comments = await findTableWithName("comments");
    return res.status(200).append("Content-Type", "application/json").end(JSON.stringify(comments));
  } catch { err } {
    console.log(err);
    res.status(500).send(err);
  }
});

/**
 * Request a post's comments by providing an ID.
 */
get.all("/post/:pid/comments", async (req, res, err) => {
  try {
    const comments = await findCommentsWithID(req.package.pid);
    if (comments) {
      // If found return the post
      return res.status(200).append("Content-Type", "application/json").end(JSON.stringify(comments));
    } else {
      // Returns a not found status
      return res.status(404).send();
    }
  } catch { err } {
    console.log(err);
    res.status(500).send(err);
  }
});


////////////////////////////////////////
/////////////// BLOCKED ////////////////
////////////////////////////////////////

/**
 * Request all blocked posts from the DB.
 */
get.all("/allBlockedPosts", async (res, err) => {
  try {
    const bPosts = await findTableWithName("blockedposts");
    return res.status(200).append("Content-Type", "application/json").end(JSON.stringify(bPosts));
  } catch { err } {
    console.log(err);
    res.status(500).send(err);
  }
});

/**
 * Request all comments from the DB.
 */
get.all("/allBlockedComments", async (res, err) => {
  try {
    const bComments = await findTableWithName("blockedcomments");
    return res.status(200).append("Content-Type", "application/json").end(JSON.stringify(bComments));
  } catch { err } {
    console.log(err);
    res.status(500).send(err);
  }
});


export default get;