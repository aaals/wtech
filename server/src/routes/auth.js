// This file is irrelevant to the code right now
// I was using it to see if it was possible to move all routing
// To an external file
import express from 'express';
// in brackets because it is a named import
import {
    authLoginController,
    authLogoutController,
    authRegController
} from '../controllers/auth.js';

var auth = express.Router();

auth.all("/login", authLoginController);
auth.all("/logout", authLogoutController);
auth.all("/register", authRegController);

export default auth;