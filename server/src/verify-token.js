import jwt from 'jsonwebtoken';
import {findUserWithID} from './controllers/find.js';

const CLIENT = "http://localhost:8080"

let verify = async (req, res, next) => {
  var token = req.cookies.myCookie || '';
  try {

    if (token === '') {
      // Not logged in
      res.status(404).send();
    }

    console.log(token);
    const decrypt = await jwt.verify(token, 'tokenPassWord');
    const uid = decrypt.uid;
    const user = await findUserWithID(uid).then();


    if(!user) {
      // Invalid or expired token
      return res.status(403).send();
    } else { 
      req.user = user;
      return next(); 
    }

  // Catch verifyToken error
  } catch (err) {
    return res.status(500).json(err.toString());
  }
};

export default verify;