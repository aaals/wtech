// Import DB element
import db from '../DB.js';

// Import object packager
import userProfile from './objects/profile.js';

const connection = await db;

/**
 * Finds a user with their user ID.
 * 
 * @param {*} uid       - The user's ID, to be used for SQL-queries, in this case to find a user.
 */
export async function findUserWithID (uid) {
    const res = await connection.query(`SELECT * FROM users WHERE uid = "${uid}"`);
    if (!res) {
      return null;
    }
    const user = userProfile(res[0]);
    
    // Returns a user's profile if found
    return user;
}

/**
 * Finds a user with their by searching for their email.
 * 
 * @param {*} email     - The user's email, to be used for SQL-queries, in this case to find a user.
 */
export function findUserWithEmail(email) {
  const res = connection.query(`SELECT * FROM users WHERE email = "${email}"`);
  if (!res) {
    return null;
  }
  const user = userProfile(res[0]);

  // Returns a user's profile if found
  return user;
}

/**
 * Finds a post with its ID.
 * 
 * @param {*} pid       - The post's post ID, to be used for SQL-queries, in this case to find a post.
 */
export async function findPostWithID(pid) {
  const table = await connection.query(`SELECT * FROM posts WHERE pid = "${pid}"`);
  if (!table) {
    return null;
  }
  // Returns a post if found
  return table[0];
}

/**
 * Finds a blocked post using its ID.
 * NOTE that all blocked post table id's (bid) match their respective post table id's (pid).
 * 
 * @param {*} bid       - The blocked post's ID, to be used for SQL-queries, in this case to find a blocked post.
 */
export function findBlockedPostWithID(bid) {
  const res = connection.query(`SELECT * FROM blockedposts WHERE bid = "${bid}"`);
  if (!res) {
    return null;
  }

  // Returns a blocked post's content if found
  return res;
}

/**
 * Find comments for a post.
 * 
 * @param {*} pid       - The post's post ID, to be used for SQL-queries, in this case to find the linked comments.
 */
export function findCommentsWithID(pid) {
  const res = connection.query(`SELECT * FROM comments WHERE post = "${pid}"`);
  if (!res) {
    return null;
  }

  // Returns the desired post's comments if found
  return res;
}

/**
 * Finds a table using its name and returns all of its contents.
 * 
 * @param {*} name      - The table's name, to be used for SQL-queries, in this case to return its entire contents.
 */
export async function findTableWithName (name) {
  const table = await connection.query(`SELECT * FROM ${name}`);
  console.log(table)
  return table;
};

export default null;