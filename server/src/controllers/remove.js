// Import DB element
import db from '../DB.js';

const connection = await db;

/**
 * Removes a user from the DB using their uid.
 * 
 * @param {*} uid       - The user's ID, to be used for SQL-queries, in this case to delete a user.
 */
export function removeUserWithID (uid) {
    connection.query(`DELETE FROM users WHERE uid = "${uid}"`);
}

/**
 * Removes a post from the DB with it's 'pid',
 * only in the case where a user is the owner 'uid'.
 * 
 * @param {*} pid       - The post's ID, to be used for SQL-queries, in this case to delete a post.
 * @param {*} uid       - The user's ID, to be used for SQL-queries, in this case to delete a post.
 */
export function removePostWithID (pid, uid) {
  connection.query(`DELETE FROM posts WHERE pid = "${pid}" AND user = "${uid}"`);
}

/**
 * Removes a comment from the DB with it's 'pid'.
 * 
 * @param {*} pid       - The post's ID, to be used for SQL-queries, in this case to delete the comments connected to a post.
 */
export function removeCommentWithID (pid) {
  connection.query(`DELETE FROM comments WHERE post = "${pid}"`);
}

/**
 * Removes a blocked comment from the DB using its 'bid'.
 * 
 * @param {*} bid       - The blocked comment's ID, to be used for SQL-queries, in this case to delete it.
 */
export function removeBlockedCommentWithID (bid) {
  connection.query(`DELETE FROM blockedcomments WHERE bid = "${bid}"`);
}

/**
 * Removes a blocked post from the DB using its 'bid'
 * 
 * @param {*} bid       - The blocked post's ID, to be used for SQL-queries, in this case to delete it.
 */
export function removeBlockedPostWithID (bid) {
  connection.query(`DELETE FROM blockedposts WHERE bid = "${bid}"`);
}


export default null;