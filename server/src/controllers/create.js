// Import DB element
import db from '../DB.js';

const connection = await db;


/**
 * Creates a user in the DB with email and password parameters.
 * NOTE: Should only be used by the auth controller.
 * 
 * @param {*} email     - The email data that's inserted into the relevant part of the SQL-table.
 * @param {*} password  - The password data that's inserted into the relevant part of the SQL-table.
 */
export function createUser(email, password) {
  connection.query(`INSERT INTO users SET email = "${email}", password = "${password}"`);
}

/**
 * Creates a post in the DB, using parameters title, content and uid.
 * 
 * @param {*} title     - The post title data that's inserted into the relevant part of the SQL-table.
 * @param {*} content   - The post content data that's inserted into the relevant part of the SQL-table.
 * @param {*} uid       - The post uid data that's inserted into the relevant part of the SQL-table.
 */
export async function createPost(title, content, uid) {
  await connection.query(`INSERT INTO posts SET user = "${uid}", title = "${title}", content = "${content}"`);
}

/**
 * Creates a blocked post in the DB, using parameters 'pid', the 'user' blocking and the 'reason'.
 * 
 * @param {*} pid       - The blocked post's pid data that's inserted into the relevant part of the SQL-table.
 * @param {*} uid       - The blocked post's uid data that's inserted into the relevant part of the SQL-table.
 * @param {*} reason    - The blocked post's reason data (e.g. why the post was blocked) that's inserted into the relevant part of the SQL-table.
 */
export async function createBlockedPost(pid, uid, reason) {
  connection.query(`INSERT INTO blockedposts SET bid = "${pid}", user = "${uid}", reason = "${reason}"`);
}

/**
 * Creates a comment in the DB, using parameters pid, uid and comment.
 * 
 * @param {*} pid       - The comments's pid data that's inserted into the relevant part of the SQL-table.
 * @param {*} uid       - The comments's uid data that's inserted into the relevant part of the SQL-table.
 * @param {*} comment   - The comments's text body data that's inserted into the relevant part of the SQL-table.
 */
export function createComment(pid, uid, comment) {
  connection.query(`INSERT INTO comments SET post = "${pid}", uid = "${uid}", comment = "${comment}"`);
}

/**
 * Creates a blocked comment in the DB, using parameters pid, the user blocking and the reason.
 * 
 * @param {*} pid       - The blocked comments's pid data that's inserted into the relevant part of the SQL-table.
 * @param {*} uid       - The blocked comments's pid data that's inserted into the relevant part of the SQL-table.
 * @param {*} reason    - The blocked comments's pid data that's inserted into the relevant part of the SQL-table.
 */
export function createBlockedComment(pid, uid, reason) {
  connection.query(`INSERT INTO blockedcomments SET bid = "${pid}", user = "${uid}", reason = "${reason}"`);
}

export default null;