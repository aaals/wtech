// Import DB element
import db from '../DB.js';

const connection = await db;


/**
 * Edits the blockID value in a post to set it as blocked.
 * 
 * @param {*} pid       - The pid data to be found and updated in the relevant part of the SQL-table.
 */
export function blockPost (pid) {
  connection.query(`UPDATE posts SET blockedID = "${pid}" WHERE pid = "${pid}"`);
}

/**
 * Edits the blockID value in a post to set it so that it's no longer blocked.
 * 
 * @param {*} pid       - The pid data to be found and updated in the relevant part of the SQL-table.
 */
export function unblockPost (pid) {
  connection.query(`UPDATE posts SET blockedID = NULL WHERE pid = "${pid}"`);
}

/**
 * Edits the blockID value in a comment to set it as blocked.
 * 
 * @param {*} cid       - The cid data to be found and updated in the relevant part of the SQL-table.
 */
export function blockComment (cid) {
  connection.query(`UPDATE comments SET blockedID = "${cid}" WHERE cid = "${cid}"`);
}

/**
 * Edits the blockID value in a comment to set it so that it's no longer blocked.
 * 
 * @param {*} cid       - The cid data to be found and updated in the relevant part of the SQL-table.
 */
export function unblockComment (cid) {
  connection.query(`UPDATE comments SET blockedID = NULL WHERE cid = "${cid}"`);
}

export default null;