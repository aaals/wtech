/**
 * Function that 'packages' profile details from, a user into objects and returns them.
 * 
 * @param {*} req		- Makes a request.
 */
export default function userProfile(req) {
	return {
		uid: req.uid,
		email: req.email,
		password: req.password,
		userType: req.userType
	};
};