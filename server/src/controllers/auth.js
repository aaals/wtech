import jwt from 'jsonwebtoken';       // Cookies
import bcrypt from 'bcryptjs';        // Hashing
// Import DB element
import db from '../DB.js';

import {createUser} from './create.js';

const connection = await db;


/**
 * Login function receives user authentication data and 
 * checks for matching username/email and password.
 * Gives message/feedback if login succeeds/fails.
 * 
 * @param {*} req - Makes a request.
 * @param {*} res - Makes a response.
 */
export async function authLoginController (req, res) {
    var email = req.body.email;
    var password = req.body.password;

     // Send error status if email or password missing
    if (!email || !password) {
      return res.status(401).send();
    }
    const connection = await db;
  
    await connection.query('SELECT * FROM users WHERE email = ?', email, async(error, results) => {
      try {
      console.log(results);
      // Send error status if user doesn't exist or password is incorrect
      if(!results || !(await bcrypt.compare(password, results[0].password))) {
        return res.status(401).send();
      } else {
        // Creating cookie
        const id = results[0].uid;  // Extract uid
        
        // Sign cookie with the uid, a password and options
        const token = jwt.sign({uid: id}, 'tokenPassWord', {
          expiresIn: '90d'
        });

        console.log("The token is: " + token);

        // Configure options read by browser
        const cookieOptions = {
          expires: new Date(
            Date.now() + 90 * 24 * 60 * 60 * 1000
          ),
          httpOnly: true
        }

        // If user is found and credentials match DB, return the generated cookie
        // and a status code for login success.
        return res.cookie('myCookie', token, cookieOptions).status(200).send();
      }
    } catch {error} {
      console.log(error);
    }
    });

}

/**
 * Logout logs user out by expiring their token.
 * 
 * @param {*} req - Makes a request.
 * @param {*} res - Makes a response.
 * @param {*} err - Holds/logs eventual errors.
 */
export function authLogoutController (req, res, err) {
  try {
    if (!req.cookies.myCookie) {
      // Return not logged in status
      return res.status(200).send();
    } else {
      // Since we're using a stateless token authentication method
      // We don't need to set up a 'database' of who is logged in/out
      // That is why when a user logs out, we can simply sign a new token
      // With an expired date to count as logging out :)

      var signedOut = 0;

      // remove info that can identify user from expired token
      var token = jwt.sign({signedOut}, 'tokenPassWord', {
        expiresIn: '0d'
      });

      console.log("The token is: " + token);

      // Set expiration to date in the past
      const cookieOptions = {
        expires: new Date(0),
        httpOnly: true
      }

      // Update existing cookie with expired one and return status for success
      return res.cookie('myCookie', token, cookieOptions).status(200).send();
    }

  } catch {err} {
    console.log(err);
  }
}

/**
 * The register function receives user authentication data and checks for duplicates in database (email).
 * Returns message if registration succeeds/fails.
 * 
 * @param {*} req - Makes a request.
 * @param {*} res - Makes a response.
 */
export function authRegController (req, res) {
  var email = req.body.email;
  var password = req.body.password;

  // Checks if email input already exists
  connection.query('SELECT email FROM users WHERE email = ?', email, async (error, results) => {
    try {
      // Checks if email/password already exist
      if (results.length > 0) {
        // Return status if email they typed is in use
        return res.status(403).send();
      }

      let hashedPassword = await bcrypt.hash(password, 8);
      console.log(hashedPassword);
      
      createUser(email, hashedPassword);
      // Return status if user successfully created
      return res.status(200).send();

      } catch {error} {
        console.log(error);
      } 
  });

}

export default null;